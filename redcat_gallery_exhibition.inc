<?php

class RedcatGalleryMigration extends Migration {
  public function __construct() {
    parent::__construct();

    $this->description = t('Migrate REDCAT Gallery Exhibitions');
    
    $source_fields = array(
      'nid' => t('The node ID of the page'),
      'front_page_image' => t('The front page image representing the gallery'),
      'right_side_images' => t('The associated images that were on the right side'),
      'linked_files' => t('The set of linked files'),
      'event_date_range' => t('The date range'),
      'event_dates' => t('The individual dates of the event'),
      'terms' => t('The terms for the node'),
    );

    $query = db_select(REDCAT_MIGRATION_DATABASE_NAME .'.node', 'n')
      ->fields('n', array('nid', 'vid', 'type', 'language', 'title', 'uid', 'status', 'created', 'changed', 'comment', 'promote', 'moderate', 'sticky', 'tnid', 'translate'))
      ->condition('n.type', 'gallery_exhibition', '=');
    // Get user information
    $query->join(REDCAT_MIGRATION_DATABASE_NAME .'.users', 'u', 'n.uid = u.uid');
    $query->addField('u', 'name');
    // content_field_series
    $query->join(REDCAT_MIGRATION_DATABASE_NAME .'.content_field_series', 'cfs', 'n.vid = cfs.vid');
    $query->addField('cfs', 'field_series_value');
    // content_field_artist
    $query->join(REDCAT_MIGRATION_DATABASE_NAME .'.content_field_artist', 'cfa', 'n.vid = cfa.vid');
    $query->addField('cfa', 'field_artist_value');
    // content_field_title_brief
    $query->join(REDCAT_MIGRATION_DATABASE_NAME .'.content_field_title_brief', 'cftb', 'n.vid = cftb.vid');
    $query->addField('cftb', 'field_title_brief_value');
    // content_field_full_artist_title
    $query->join(REDCAT_MIGRATION_DATABASE_NAME .'.content_field_full_artist_title', 'cffat', 'n.vid = cffat.vid');
    $query->addField('cffat', 'field_full_artist_title_value');
    // content_field_subtitle
    $query->join(REDCAT_MIGRATION_DATABASE_NAME .'.content_field_subtitle', 'cfsub', 'n.vid = cfsub.vid');
    $query->addField('cfsub', 'field_subtitle_value');
    // content_field_premiere
    $query->join(REDCAT_MIGRATION_DATABASE_NAME .'.content_field_premiere', 'cfp', 'n.vid = cfp.vid');
    $query->addField('cfp', 'field_premiere_value');
    // content_field_copresentation_credit
    $query->join(REDCAT_MIGRATION_DATABASE_NAME .'.content_field_copresentation_credit', 'cfcc', 'n.vid = cfcc.vid');
    $query->addField('cfcc', 'field_copresentation_credit_value');
    // content_field_pull_quotes
    $query->join(REDCAT_MIGRATION_DATABASE_NAME .'.content_field_pull_quotes', 'cfpq', 'n.vid = cfpq.vid');
    $query->addField('cfpq', 'field_pull_quotes_value');
    // content_field_funding_credit
    $query->join(REDCAT_MIGRATION_DATABASE_NAME .'.content_field_funding_credit', 'cffc', 'n.vid = cffc.vid');
    $query->addField('cffc', 'field_funding_credit_value');
    // content_field_node_body
    $query->join(REDCAT_MIGRATION_DATABASE_NAME .'.content_field_node_body', 'cfnb', 'n.vid = cfnb.vid');
    $query->addField('cfnb', 'field_node_body_value');
    // content_field_node_teaser
    $query->join(REDCAT_MIGRATION_DATABASE_NAME .'.content_field_node_teaser', 'cfnt', 'n.vid = cfnt.vid');
    $query->addField('cfnt', 'field_node_teaser_value');
    $query->orderBy('n.changed');
    
    $this->highwaterField = array(
      'name' => 'changed', // Column to be used as highwater mark
      'alias' => 'n',           // Table alias containing that column
    );
    
    $this->source = new MigrateSourceSQL($query, $source_fields);
    $this->destination = new MigrateDestinationNode('gallery_exhibition');

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'nid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'D6 Unique Node ID',
          'alias' => 'n',
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );
    
    // Make the mappings
    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('is_new')->defaultValue(TRUE);
    $this->addFieldMapping('uid', 'uid');
    $this->addFieldMapping('revision')->defaultValue(TRUE);
    $this->addFieldMapping('revision_uid', 'uid');
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('changed', 'changed');
    $this->addFieldMapping('status', 'status');
    $this->addFieldMapping('promote', 'promote');
    $this->addFieldMapping('sticky', 'sticky');
    $this->addFieldMapping('comment', 'comment');
    $this->addFieldMapping('language')->defaultValue('und');
    $this->addFieldMapping('path')->issueGroup(t('DNM'));
    $this->addFieldMapping('pathauto_perform_alias')->defaultValue('1');

    // Add all the CCK associations
    $this->addFieldMapping('field_content_series', 'field_series_value');
    $this->addFieldMapping('field_content_artist_company', 'field_artist_value');
    $this->addFieldMapping('field_content_event_type', 'terms')->separator(',');
    $this->addFieldMapping('field_content_title_brief', 'field_title_brief_value');
    $this->addFieldMapping('field_content_subtitle', 'field_subtitle_value');
    $this->addFieldMapping('field_content_premiere', 'field_premiere_value');
    
    $generic_textarea_arguments = MigrateTextFieldHandler::arguments(NULL, filter_default_format());
    
    $this->addFieldMapping('field_content_copres_credit', 'field_copresentation_credit_value')
      ->arguments($generic_textarea_arguments);
    
    $this->addFieldMapping('field_content_pull_quotes', 'field_pull_quotes_value')
      ->arguments($generic_textarea_arguments);

    $this->addFieldMapping('field_content_funding_credit', 'field_funding_credit_value')
      ->arguments($generic_textarea_arguments);

    $this->addFieldMapping('field_content_full_artist_title', 'field_full_artist_title_value')
      ->arguments($generic_textarea_arguments);
    
    $this->addFieldMapping('field_content_teaser', 'field_node_teaser_value')
      ->arguments($generic_textarea_arguments);

    $this->addFieldMapping('field_content_body', 'field_node_body_value')
      ->arguments($generic_textarea_arguments);

    $file_arguments = MigrateFileFieldHandler::arguments(NULL, 'file_copy', FILE_EXISTS_REPLACE);
    
    $this->addFieldMapping('field_content_front_page_image', 'front_page_image')
      ->arguments($file_arguments);

    $this->addFieldMapping('field_content_associated_images', 'right_side_images')
      ->arguments($file_arguments);

    $this->addFieldMapping('field_content_linked_files', 'linked_files')
      ->arguments($file_arguments);
    
    $generic_date_arguments = RedcatMigrateDateFieldHandler::arguments(0, 1, NULL, '|');
    $this->addFieldMapping('field_content_date_range', 'event_date_range')
      ->arguments($generic_date_arguments);

    $this->addFieldMapping('field_content_date_time', 'event_dates')
      ->separator('##')
      ->arguments($generic_date_arguments);

    $this->addFieldMapping(NULL, 'name');
    $this->addFieldMapping(NULL, 'vid');
    $this->addFieldMapping(NULL, 'type');
    $this->addFieldMapping(NULL, 'language');
    $this->addFieldMapping(NULL, 'moderate');
    $this->addFieldMapping(NULL, 'tnid');
    $this->addFieldMapping(NULL, 'translate');
  }
  
  public function prepareRow($current_row) {
    $current_row->uid = $current_row->revision_uid = redcat_migration_retrieve_uid($current_row->name);
    $current_row->terms = redcat_migration_retrieve_terms($current_row->nid);
    $current_row->front_page_image = redcat_migration_retrieve_front_page_image($current_row->nid);
    $current_row->right_side_images = redcat_migration_retrieve_right_side_images($current_row->nid);
    $current_row->linked_files = redcat_migration_retrieve_linked_files($current_row->nid);
    
    $current_row->event_date_range = redcat_migration_retrieve_date_range($current_row->nid);
    $current_row->event_dates = redcat_migration_retrieve_date_times($current_row->nid);

    return TRUE;
  }
}
